from django.shortcuts import render
from django.views.generic import TemplateView, CreateView, View
from .models import Tiuser

class IndexView(TemplateView):
    """Clase Index"""
    template_name = "index.html"

class TiuserCreate(CreateView):
    template_name = "tiuser_form.html"
    model = Tiuser
    fields = ['id','ntiuser','default']
    
class TiuserCreate(CreateView):
    template_name = "tiuser_form.html"
    model = Tiuser
    fields = ['id','ntiuser','default']