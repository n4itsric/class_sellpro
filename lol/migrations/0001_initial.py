# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Tiuser'
        db.create_table(u'tiuser', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ntiuser', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
            ('default', self.gf('django.db.models.fields.CharField')(max_length=3, blank=True)),
        ))
        db.send_create_signal(u'lol', ['Tiuser'])


    def backwards(self, orm):
        # Deleting model 'Tiuser'
        db.delete_table(u'tiuser')


    models = {
        u'lol.tiuser': {
            'Meta': {'object_name': 'Tiuser', 'db_table': "u'tiuser'"},
            'default': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ntiuser': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        }
    }

    complete_apps = ['lol']