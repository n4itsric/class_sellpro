# -*- encoding: utf-8 -*-

from django.db import models

# Create your models here.

class Tiuser(models.Model):
    id = models.AutoField(primary_key=True,verbose_name='Código')
    ntiuser = models.CharField(max_length=150, blank=True,verbose_name='Nombre')
    CHOICES_DEFAULT=(
        ('S','Utilizar Usuario creacion por defecto'),
        ('N','No Utilizar Usuario creacion por defecto'),
    )
    default = models.CharField(max_length=3, blank=True,verbose_name='Default',choices=CHOICES_DEFAULT)
    class Meta:
        db_table = u'tiuser'
        verbose_name_plural = "Tipos de Usuario"
    def __unicode__(self):
        return u'%s' % (self.ntiuser)